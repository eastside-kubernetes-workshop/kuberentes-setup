#!/bin/bash
set -eux -o pipefail

# Setup System
# TODO: Make all of these persist.
#TODO: Set this in etc sysctl?
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward
#TODO: Set this up at startup?
sudo modprobe br_netfilter
sudo swapoff -a
