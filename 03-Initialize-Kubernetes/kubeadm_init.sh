#!/bin/bash 
set -eux -o pipefail
# Prerequisites: software_requirements.sh system_setup.sh
# Initial setup of the cluster.
# https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

kubeadm config images pull
sudo kubeadm init

# At this point, Kubeadm will output a success message, stating that the cluster has been successfully initialized.
# Your Kubernetes control-plane has initialized successfully!
# 
# To start using your cluster, you need to run the following as a regular user:
# 
#   mkdir -p $HOME/.kube
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config
# 
# You should now deploy a pod network to the cluster.
# Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
#   https://kubernetes.io/docs/concepts/cluster-administration/addons/
# 
# Then you can join any number of worker nodes by running the following on each as root:
# 
# kubeadm join 10.1.10.133:6443 --token avckzm.db2hq8vnvla2ct4f \
#     --discovery-token-ca-cert-hash sha256:fcdcf2385f249550c28ea3a7d934c43eb0faf3334d2e4657211d33a502940236

# From kubedadm init output:
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
