#!/bin/bash 
set -eux -o pipefail
# Prerequisites: kubeadm_init.sh

curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml
