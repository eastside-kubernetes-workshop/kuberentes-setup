# Intro
This is a set of notes on a bare-metal kubernetes cluster.. Each folder will contain a set of detailed instructions on installing bits and pieces until we have a full kubernetes installation.

## Steps;
1) Install OS.
Requirements: A bare piece of hardware. The assumption in this document is that this is long-lived physical hardware, but not much changes if it's virtual hardware until you get to the storage layer.
Expected end state: A linux server
2) Install Kubernetes components
Requirments: A linux server
Expected end state: A kubernetes node
3) Initialize Kubernetes
Requirements: A kubernetes node
Expected end state: A functioning kubernetes master
4) Add nodes
Requirements: A functioning Kubernetes master, several kubernetes nodes
Expected end state: A functioning kubernetes cluster
