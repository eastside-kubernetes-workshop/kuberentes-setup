# Steps:
## 1 Create bootable image ona usb drive
 * down The Ubuntu server 18.04 LTS ISO file 
 * Use your OS's utility to write the ISO to a USB drive
## 2 Boot laptop from USB
 * for the Dell laptops press F12 at boot time
 * Choose the USB choices (if it does not show  USB Option, use the USB 2.0 drive)
## 3 Install
 * Hook up your machine and run through ubuntu installation.
 * Defaults, and the things you have to set:
 * Your Name: `server`
 * Your server's name: `serv`NN *(where NN is the number from the sticky note)*
 * Username: `server`
 * Password: `server`
 * install OpenSSH Server: `yes`
 * set up publickeys with github: `gauntletwizard`
 * Allow passowrd authentication over SSH: `yes`

## After reboot

If the system will not boot because a partition table is invalid,
you need to setup a Hybrid Parition Table

Boot and choose `Install Ubuntu`
At the first menu pres the `ESC` key

### Setup Hybrid Partition table:
Set up hybrid mbr - this is a hack. These machines are (mostly) old enough 
not to support gpt partition type.
```
gdisk /dev/sda
    r
    p
    h
    1 2
    Y
    <Default>
    Y
    <Default>
    N
    Y
    <Default>
    w
    y
  reboot
```



## Post-install hooks:
### Setup authorized_keys to allow login:
```
curl https://github.com/gauntletwizard.keys |ctee -a ~/.ssh/authorized_keys
curl https://github.com/mhahnciber.keys |ctee -a ~/.ssh/authorized_keys
```
